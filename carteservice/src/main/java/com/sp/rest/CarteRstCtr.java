package com.sp.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sp.model.Carte;
import com.sp.service.CarteService;

@RestController
@RequestMapping("/cartes")
public class CarteRstCtr {
	
	@Autowired
	private CarteService cService;
	
	/**
	 * But : Par défaut retourne la liste de toutes les cartes
	 * @return List<Carte>
	 */
	@GetMapping
	public List<Carte> getAllCartes() {
		return cService.getAllCartes();
	}
	
	/**
	 * But : Retourne carte par son id
	 * @param id1
	 * @return
	 */
	@GetMapping(value="/{id1}")
	public Carte getCarteById(@PathVariable String id1) {
		Carte carteAafficher = cService.getCarteById(Integer.valueOf(id1));
		return carteAafficher;
	}
	
	/**
	 * But : Retourne le prix d'une carte par son id
	 * @param id1
	 * @return price
	 */
	@GetMapping(value="/prices/{id1}")
	public int getPrice(@PathVariable String id1) {
		return cService.getCarteById(Integer.valueOf(id1)).getPrice();
	}
	
	/** 
	 * But : Retourne la carte via son nom 
	 * @param search
	 * @return
	 */
	@GetMapping(value="/name/{search}")
	public  Carte getCarteByName(@PathVariable String search){
		Carte carte = cService.getCardByName(search);
		return carte;
	}
	
	/**
	 * But : Ajout d'une carte (Optionnel car personne doit le faire)
	 * @param carte
	 */
	@PostMapping
	public void addCartes(@RequestBody Carte carte) {
		cService.addCarte(carte);
		System.out.println(carte);
	}
	
	/**
	 * But :Modification du propriétaire de la carte 
	 * @param idCarte
	 * @param idOwner
	 */
	@PutMapping
	public void modifOwner(@RequestParam int idCarte, @RequestParam int idOwner) {
		cService.modificateOwner(idCarte, idOwner);
	}
	
	/**
	 * But : Retourne 5 cartes via l'id  
	 * @param idOwner
	 * @return
	 */
	@GetMapping(value="/getFiveCards/{idOwner}")
	public  List<Carte> getFiveCards(@PathVariable String idOwner){
		return cService.getCinqCartes(Integer.valueOf(idOwner));
		
	}
	
	/**
	 * But : Création d'une base de données de cartes 
	 * @return
	 */
	@GetMapping(value="/construction")
	public String addCartesMultiple() {
		Carte carte = new Carte("DC Comic","https://static.fnac-static.com/multimedia/Images/8F/8F/7D/66/6716815-1505-1540-1/tsp20171122191008/Lego-lgtob12b-lego-batman-movie-lampe-torche-batman.jpg","BATMAN","Bruce Wayne, alias Batman, est un héros de fiction ",50,80,170,80,100);
		cService.addCarte(carte);
		carte = new Carte("Marvel","pasdimage","Superman","il est très fort",50,800,170,80,10);
		cService.addCarte(carte);
		carte = new Carte("CPE","pasdimage","Green Lantern","Le film est pas fou",10,400,70,80,10);
		cService.addCarte(carte);
		carte = new Carte("OUO","pasdimage","ouais ouais ouais","un deux",50,800,170,80,10);
		cService.addCarte(carte);
		carte = new Carte("Magic","pasdimage","bourrasque","inflige 2 blessures",10,500,50,60,10);
		cService.addCarte(carte);
		carte = new Carte("Magic","pasdimage","Ile","Terrain de base",50,800,170,80,10);
		cService.addCarte(carte);
		carte = new Carte("Magic","pasdimage","Oriflamme orque","gagne +1",10,400,70,80,10);
		cService.addCarte(carte);
		carte = new Carte("Magic","pasdimage","Douleur flamboyante","blessures a l'adversaires",50,800,170,80,10);
		cService.addCarte(carte);
		carte = new Carte("Magic","pasdimage","Resolution heroique","ajoute +1 au stat",50,800,170,80,10);
		cService.addCarte(carte);
		carte = new Carte("Magic","pasdimage","Nahiri","Panswalker ",50,800,170,80,10);
		cService.addCarte(carte);
		carte = new Carte("Magic","pasdimage","Dompteur de kronch","Piétinement 2/1",10,500,30,90,100);
		cService.addCarte(carte);
		carte = new Carte("Magic","pasdimage","Guerrier Elfe","terribles combattans",50,800,170,80,10);
		cService.addCarte(carte);
		carte = new Carte("Magic","pasdimage","Elemental de feu ","il frappe des volcans",50,800,170,80,10);
		cService.addCarte(carte);
		carte = new Carte("Yugioh","pasdimage","La sentinelle pourpre","Sacrifiez 1 pour détruire",150,600,80,80,10);
		cService.addCarte(carte);
		carte = new Carte("Yugioh","pasdimage","Gozuki","Zombie",50,810,170,30,10);
		cService.addCarte(carte);
		carte = new Carte("Yugioh","pasdimage","SteamRoid","Train technique",50,800,170,80,10);
		cService.addCarte(carte);
		carte = new Carte("Yugioh","pasdimage","JetRoid","Piétinement 2/1",10,500,30,90,100);
		cService.addCarte(carte);
		carte = new Carte("Yugioh","https://storage.googleapis.com/ygoprodeck.com/pics/21844576.jpg","Avian","Un héro élementaire",50,800,170,80,10);
		cService.addCarte(carte);
		carte = new Carte("Yugioh","https://storage.googleapis.com/ygoprodeck.com/pics/46986414.jpg","Kumori dragon","un dragon vicueux",50,800,170,80,10);
		cService.addCarte(carte);
		carte = new Carte("Yugioh","https://storage.googleapis.com/ygoprodeck.com/pics/6983839.jpg","Dragon Tornade","XYZ niveau 4",50,800,170,80,10);
		cService.addCarte(carte);
		carte = new Carte("Yugioh","https://storage.googleapis.com/ygoprodeck.com/pics/71625222.jpg","Magicien du temps","faite un pile ou face. Pile : tu gagne. Face : tu perd.",50,50,170,80,10);
		cService.addCarte(carte);
		carte = new Carte("Yugioh","https://storage.googleapis.com/ygoprodeck.com/pics/77585513.jpg","Jinzo","Desactivez les magies et pièges.",150,600,80,80,10);
		cService.addCarte(carte);
		carte = new Carte("Yugioh","https://storage.googleapis.com/ygoprodeck.com/pics/78193831.jpg","Buster Blader","Gagne 500 par Dragon",500,10,170,30,150);
		cService.addCarte(carte);
		carte = new Carte("Yugioh","https://storage.googleapis.com/ygoprodeck.com/pics/55144522.jpg","Pot de Cupidité","Piochez + 2 cartes",10,10,10,10,100);
		cService.addCarte(carte);
		carte = new Carte("Yugioh","https://storage.googleapis.com/ygoprodeck.com/pics/14558127.jpg","Ash","Annule un effet",10,500,30,90,80);
		cService.addCarte(carte);
		carte = new Carte("Yugioh","https://storage.googleapis.com/ygoprodeck.com/pics/79979666.jpg","Burstinatrix","Un héro élementaire manipulateur de flamme",1200,800,170,80,30);
		cService.addCarte(carte);
		carte = new Carte("Yugioh","https://storage.googleapis.com/ygoprodeck.com/pics/79979666.jpg","Bubble Man","il maitrise l'eau",00,1000,170,80,175);
		cService.addCarte(carte);
		carte = new Carte("Yugioh","https://storage.googleapis.com/ygoprodeck.com/pics/6983839.jpg","Dragon Tornade","XYZ niveau 4",50,800,170,80,10);
		cService.addCarte(carte);
		carte = new Carte("Yugioh","https://storage.googleapis.com/ygoprodeck.com/pics/70095154.jpg","Cyber Dragon","Spé depuis la mains",50,600,180,80,60);
		cService.addCarte(carte);
		carte = new Carte("Yugioh","https://storage.googleapis.com/ygoprodeck.com/pics/8491308.jpg","Hayate","attaquez directement",50,800,170,60,20);
		cService.addCarte(carte);
		carte = new Carte("Yugioh","https://storage.googleapis.com/ygoprodeck.com/pics/26077387.jpg","Raye","Spé Hayate en sacrifiant cette carte",50,400,170,80,10);
		cService.addCarte(carte);
		carte = new Carte("Yugioh","https://storage.googleapis.com/ygoprodeck.com/pics/12421694.jpg","Hayate","Puissant assaut de l'air",50,50,170,80,40);
		cService.addCarte(carte);
		carte = new Carte("Yugioh","https://storage.googleapis.com/ygoprodeck.com/pics/97268402.jpg","Effect Vailer","Discard and cancel an effet",400,80,100,30,80);
		cService.addCarte(carte);

		return "Ajout des cartes";
	}
}