package com.sp.model;

import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class CarteTest {
	private List<String> stringList;
	private List<String> familyList;
	private List<Integer> intList;
	
	@Before
	public void setUp() {
		System.out.println("[BEFORE TEST] -- Add Carte to test");
		stringList = new ArrayList<String>();
		intList = new ArrayList<Integer>();
		familyList = new ArrayList<String>();
		familyList.add("Pokemon");
		familyList.add("Digimon");
		familyList.add("Tamagochi");
		stringList.add("a");
		stringList.add("b");
		stringList.add("ab");
		stringList.add("c");
		stringList.add("d");
		stringList.add("cd");
		stringList.add("ac");
		stringList.add("bc");
		stringList.add("ad");
		stringList.add("bd");
		for (int i=1; i < 10;i++) {
			intList.add(i);
		}
	}
	
	@After
	public void tearDown() {
		System.out.println("[AFTER TEST] -- CLEAN carte list");
		stringList = null;
		intList = null;
	}
	
	@Test
	public void createCarte() {
		int size=familyList.size();
		for (Integer i=0; i < size;i++) {
			for (Integer msg:intList) {
				for (Integer msg1:intList) {
					for (Integer msg2:intList) {
						for (Integer msg3:intList) {
							for (Integer msg4:intList) {
								for (String msg5:stringList) {
									for (String msg6:stringList) {
										for (String msg7:stringList) {
											Carte c=new Carte(familyList.get(i), msg5, msg6, msg7, msg, msg1, msg2, msg3, msg4);
											assertTrue(c.getFamily_name() == familyList.get(i));
											assertTrue(c.getName() == msg6);
											assertTrue(c.getImg_src() == msg5);
											assertTrue(c.getDescription() == msg7);
											assertTrue(c.getHp() == msg);
											assertTrue(c.getEnergy() == msg1);
											assertTrue(c.getAttack() == msg2);
											assertTrue(c.getDefense() == msg3);
											assertTrue(c.getPrice() == msg4);
										}
									}
								}
							}
						}
					}						
				}
			}
		}
	}

	@Test
	public void displayCarte() {
		Carte c= new Carte("Marvel","pasdimage","Superman","il est très fort",50,800,170,80,10);
		String expectedResult= "{id:0, family_name:Marvel, img_src:pasdimage, name:Superman, description:il est très fort, hp:50, energy:800, attack:170, defense:80, price:10}";
		assertTrue(c.toString().equals(expectedResult));
	}
}
