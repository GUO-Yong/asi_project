package com.sp.service;

import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.sp.model.Carte;
import com.sp.repository.CarteRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(value = CarteService.class)
public class CarteServiceTest {

	@Autowired
	private CarteService cService;

	@MockBean
	private CarteRepository cRepo;
	
	Carte tmpUser=new Carte("test","test","test","test",0,0,0,0,0);
	@Test
	public void getCarte() {
		Mockito.when(
				cRepo.findById(Mockito.any())
				).thenReturn(Optional.ofNullable(tmpUser));
		Carte carteInfo=cService.getCarteById(0);
		assertTrue(carteInfo.toString().equals(tmpUser.toString()));
	}
}
