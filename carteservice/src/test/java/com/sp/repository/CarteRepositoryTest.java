package com.sp.repository;

	import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

    import java.util.ArrayList;
    import java.util.List;
    import org.junit.After;
    import org.junit.Before;
    import org.junit.Test;
    import org.junit.runner.RunWith;
    import org.springframework.beans.factory.annotation.Autowired;
    import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
    import org.springframework.test.context.junit4.SpringRunner;
    import com.sp.model.Carte;

    @RunWith(SpringRunner.class)
    @DataJpaTest
    public class CarteRepositoryTest {

        @Autowired
        CarteRepository crepo;

        @Before
        public void setUp() {
            crepo.save( new Carte("Marvel","pasdimage","Superman","il est très fort",50,800,170,80,10));
        }

        @After
        public void cleanUp() {
            crepo.deleteAll();
        }

        @Test
        public void saveCarte() {
            crepo.save( new Carte("Marvel","pasdimage","Superman","il est très fort",50,800,170,80,10));
            assertTrue(true);
        }

        @Test
        public void saveAndGetCarte() {
            crepo.deleteAll();
            crepo.save( new Carte("Marvel","pasdimage","Superman","il est très fort",50,800,170,80,10));
            List<Carte> carteList = new ArrayList<>();
            crepo.findAll().forEach(carteList::add);
            assertTrue(carteList.size() == 1);
            assertEquals(carteList.get(0).getAttack(),170);
            assertEquals(carteList.get(0).getDefense(),80);
            assertTrue(carteList.get(0).getDescription().equals("il est très fort"));
            assertEquals(carteList.get(0).getEnergy(),800);
            assertTrue(carteList.get(0).getFamily_name().equals("Marvel"));
            assertEquals(carteList.get(0).getHp(),50);
            assertTrue(carteList.get(0).getImg_src().equals("pasdimage"));
            assertTrue(carteList.get(0).getName().equals("Superman"));
            assertEquals(carteList.get(0).getPrice(),10);
        }

        @Test
        public void getCarte() {
            List<Carte> carteList = crepo.findByName("Superman");
            assertTrue(carteList.size() == 1);
            assertTrue(carteList.get(0).getName().equals("Superman"));
            assertEquals(carteList.get(0).getAttack(),170);
            assertTrue(carteList.get(0).getImg_src().equals("pasdimage"));
            
        }

        @Test
        public void findByName() {
            crepo.save(new Carte("Marvel","pasdimage","Superman","il est très fort",50,800,170,80,10));
            crepo.save(new Carte("Marvel","pasdimage","Superman","il est très fort",50,800,170,80,10));
            crepo.save(new Carte("Marvel","pasdimage","Superman","il est très fort",50,800,170,80,10));
            crepo.save(new Carte("Marvel","pasdimage","Superman","il est très fort",50,800,170,80,10));
            List<Carte> CarteList = new ArrayList<>();
            crepo.findByName("Superman").forEach(CarteList::add);
            assertTrue(CarteList.size() == 5);
        }
    }


