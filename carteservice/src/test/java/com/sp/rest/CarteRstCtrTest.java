package com.sp.rest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.sp.model.Carte;
import com.sp.service.CarteService;

@RunWith(SpringRunner.class)
@WebMvcTest(value = CarteRstCtr.class)
public class CarteRstCtrTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private CarteService cService;

	Carte mockCarte=new Carte("Marvel","pasdimage","Superman","il est très fort",50,800,170,80,10);
	
	@Test
	public void retrieveCarte() throws Exception {
		Mockito.when(
				cService.getCarteById(Mockito.anyInt())
				).thenReturn(mockCarte);
				

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/cartes/1").accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		System.out.println(result.getResponse().getContentAsString());
		String expectedResult="{\"id\":0, \"family_name\":\"Marvel\", \"img_src\":\"pasdimage\", \"name\":\"Superman\", \"description\":\"il est très fort\", \"hp\":50, \"energy\":800, \"attack\":170, \"defense\":80, \"price\":10}";
		JSONAssert.assertEquals(expectedResult, result.getResponse().getContentAsString(), false);
	}

}

