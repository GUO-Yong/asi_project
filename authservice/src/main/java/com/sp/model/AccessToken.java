package com.sp.model;

import java.nio.charset.Charset;
import java.util.Random;

public class AccessToken {
	private String token;
	public AccessToken() {}
	public AccessToken(String token) {
		this.token = token;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	
	public void generateToken() {
		// Cette partie de code a été récupérée sur https://www.baeldung.com/java-random-string
		// Elle nous permet d'obteniur un token sous forme de string composé de 7 lettres
		byte[] array = new byte[7];
	    new Random().nextBytes(array);
	    String token = new String(array, Charset.forName("UTF-8"));
	    
	    this.token = token;
	}
	
}
