package com.sp.model;

public class Auth {
	private String idconnexion;
	private String password;
	
	public Auth() {
	}
	
	public Auth(String idconnexion,String password) {
		super();
		this.idconnexion=idconnexion;
		this.password=password;
	}
	
	public String getUser() {
		return idconnexion;
	}

	public String getPassword() {
		return password;
	}

	@Override
	public String toString() {
		return "Les identifiants de l'utilisateur sont" + idconnexion + "," + password;
	}
	
}
