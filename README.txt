userapp : 8081
cardapp : 8082
transactionapp :8083
authapp : 8084
zuulapp : 8085
roomapp : 8086

Les éléments non réalisés sont : les services au Game et le Front

Les tâches réalisées PAR MEMBRE:

CLEMENT CORNU-----------------------------------------------------------------------

	-> zuulapp
		|-Redirection des url

	-> authapp
		|-Authentification d'un utilisateur avec son identifiant et mot de passe

ANTOINE CROCQUEVIEILLE--------------------------------------------------------------

	-> userapp avec tests
		|-Création/ajout des utilisateur avec cartes aléatoires
		|-Obtenir un utilisateur par Id ou par identifiant et mot de passe
		|-Obtenir la liste des cartes d'un utilisateur
		|-Obtenir tous les utilisateurs
		|-Suppression/ajout d'une carte à un utilisateur
		|-Mettre à jour la portefeuille d'un utilisateur

PHILIPPE CHARRAT--------------------------------------------------------------------

	-> cardapp avec tests
		|-Obtenir toutes les cartes
		|-Obtenir la carte par Id ou par nom 
		|-Obtenir le prix d'une carte par Id
		|-Création/ajout d'une carte
		|-Changer le propriétaire d'une carte
		|-Obtenir les cartes par Id du propriétaire 

	-> roomapp avec tests
		|-Obtenir tous les rooms 
		|-Obtenir le room par Id ou par prix
		|-Création d'un room
		|-Mettre à jour le room, s'il y a deux utilisateurs dans un même room 

YONGBIN GUO--------------------------------------------------------------------------

	-> transactionapp avec tests
		|-Obtenir toutes les transactions disponibles(les ventes)
		|-Obtenir la transaction par Id
		|-Ajout d'une nouvelle transaction(mise en vente d'une carte par un utilisateur)
		|-Terminer la transaction par Id de la transaction et Id de l'acheteur (carte vendue)
	
	-> Le dossier "DockerImages" contient les images Docker de ces différents services
	
		



