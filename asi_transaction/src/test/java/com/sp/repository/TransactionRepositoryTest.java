
package com.sp.repository;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.sp.model.Transaction;


@RunWith(SpringRunner.class)
@DataJpaTest
public class TransactionRepositoryTest {
    @Autowired
    TransactionRepository trepo;

    @Before
    public void setUp() {
		int idCarte = 0;
		int idVendeur = 0;

		Transaction h =new Transaction(idVendeur,idCarte);
        trepo.save(h );
    }

    @After
    public void cleanUp() {
        trepo.deleteAll();
    }

    @Test
    public void saveTransaction() {
		int idCarte = 1;
		int idVendeur = 1;

		Transaction h =new Transaction(idVendeur,idCarte);
    	 trepo.save(h );
        assertTrue(true);
    }

    @Test
    public void saveAndGetTransaction() {
        trepo.deleteAll();
        
		int idCarte = 1;
		int idVendeur = 2;
		Transaction h =new Transaction(idVendeur,idCarte);
        trepo.save( h);
        List<Transaction> transactionList = new ArrayList<>();
        trepo.findAll().forEach(transactionList::add);
        assertTrue(transactionList.size() == 1);
        assertTrue(transactionList.get(0).getIdVendeur() == idVendeur);
        assertTrue(transactionList.get(0).getIdCarte() == idCarte  );

    }

    @Test
    public void getTransaction() {
        
		int idCarte = 1;
		int idVendeur = 2;

		Transaction h =new Transaction(idVendeur,idCarte);
		trepo.save( h);
		
    	List<Transaction> transactionList1 = trepo.findByIdCarte(1);
        assertTrue(transactionList1.size() == 1);
        assertTrue(transactionList1.get(0).getIdCarte()== idCarte  );
        
    	List<Transaction> transactionList2 = trepo.findByIdVendeur(2);
        assertTrue(transactionList2.size() == 1);
        assertTrue(transactionList2.get(0).getIdVendeur()== idVendeur  );
        
        
    }

    @Test
    public void findByCarte() {

        trepo.save(new Transaction(1,1));
        trepo.save(new Transaction(1,1));
        trepo.save(new Transaction(1,1));
        trepo.save(new Transaction(1,2));
        List<Transaction> TransactionList = new ArrayList<>();
        trepo.findByIdCarte(1).forEach(TransactionList::add);
        assertTrue(TransactionList.size() == 3);
    }
    

    @Test
    public void findByVendeur() {
        trepo.save(new Transaction(1,1));
        trepo.save(new Transaction(1,1));
        trepo.save(new Transaction(1,1));
        trepo.save(new Transaction(2,1));
        List<Transaction> TransactionList = new ArrayList<>();
        trepo.findByIdVendeur(1).forEach(TransactionList::add);
        assertTrue(TransactionList.size() == 3);
    }
    
    
}
