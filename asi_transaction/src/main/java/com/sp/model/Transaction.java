package com.sp.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Transaction {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private int idCarte;
	private int idVendeur;
	
	
	//private User acheteur;
	
	public Transaction() {}
	
	public Transaction( int idVendeur,int idCarte) {
		super();
		this.idVendeur = idVendeur;
		this.idCarte = idCarte;
	}

	public int getIdVendeur() {
		return idVendeur;
	}

	public int getIdCarte() {
		return idCarte;
	}

	@Override
	public String toString() {
		return "[ Transaction: "+this.id+
				", Id Vendeur: "+this.idVendeur +
				", Id Carte: "+this.idCarte +" ]";
	}
	
}
