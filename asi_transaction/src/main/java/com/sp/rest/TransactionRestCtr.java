package com.sp.rest;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sp.model.Transaction;
import com.sp.service.TransactionService;


@RestController
public class TransactionRestCtr {

	@Autowired
	private TransactionService tService;
	
	//retourner toutes les transactions
	@RequestMapping(method=RequestMethod.GET,value="/transactions")
	public List<Transaction> getAllTransactions() {
		System.out.println("return all transactions");
		return tService.getAllTransactions();
	}
	
	//retourner la transaction dont id = ..
	@RequestMapping(method=RequestMethod.GET,value="/transactions/{id1}")
	public Transaction getMsg(@PathVariable String id1) {
		int id = Integer.parseInt(id1);
		Transaction transaction = tService.getTransactionById(id);
		return transaction;
	}
	
	//ajouter une nouvelle transaction(vente)
	@RequestMapping(method=RequestMethod.POST,value="/transactions")
	public void addTransaction(@RequestParam Integer idCarte,@RequestParam Integer idVendeur) {
		tService.addTransaction(new Transaction(  idVendeur, idCarte));
		

	}
	
	//terminer la transaction(vendue)
	@RequestMapping(method=RequestMethod.DELETE,value="/transactions")
	public void closeTransaction(@RequestParam Integer idTransaction,@RequestParam Integer idAcheteur) {
		tService.closeTransaction(idTransaction,idAcheteur);
		System.out.println("close a transaction");
	}
}
