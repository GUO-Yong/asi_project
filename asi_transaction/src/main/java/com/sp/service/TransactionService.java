package com.sp.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.sp.model.Transaction;
import com.sp.repository.TransactionRepository;

@Service
public class TransactionService {
	@Autowired
	TransactionRepository tRepository;
	
	//Ajouter la transaction
	public void addTransaction(Transaction transaction) {
		//ajouter la transaction
		tRepository.save(transaction);
		//supprimer la carte de l'utilisateur
		deleteUserCarte(transaction.getIdVendeur(),transaction.getIdCarte());
		System.out.println("add transaction");
		System.out.println(transaction);
		
		
	}
	
	//retourner toutes les transactions
	public List<Transaction> getAllTransactions() {
		List<Transaction> transactions = new ArrayList<>();
		tRepository.findAll().forEach(transactions::add);
	    return transactions;
	}
	
	//retourner la transaction dont id = ..
	public Transaction getTransactionById(int id) {
		Optional<Transaction> tOpt = tRepository.findById(id);
		if (tOpt.isPresent()) {
			return tOpt.get();
		}else {
			return null;
		}
	}
	
	//terminer la transaction(vendue)
	public void closeTransaction(int idTransaction,int idAcheteur) {
		Optional<Transaction> tOpt = tRepository.findById(idTransaction);
		if (tOpt.isPresent()) {
			Transaction transactionSelected = tOpt.get();
			int idVendeur = transactionSelected.getIdVendeur();
			int idCarte = transactionSelected.getIdCarte();

			int price = getCartePrice(idCarte);
			int portefeuille = getUserPortefeuille(idAcheteur);
			
			if( portefeuille>=price ) {
				//set portefeuille
				 putUserPortefeuille(idVendeur, price);  
				 putUserPortefeuille(idAcheteur, -1*price);  
				 //add carte & remove carte
				 deleteUserCarte(idVendeur,idCarte);
				 postUserCarte(idAcheteur,idCarte);
				 //changer owner
				 putOwnerCarte( idAcheteur, idCarte);
				 
				 //supprimer la transaction 
				 tRepository.deleteById(idTransaction);
				 System.out.println("close transaction: OKf");
				}
			
				
			}
		}
	

			
		
		private int getUserPortefeuille(int id) {
			ResponseEntity<UserDTO> resp = new RestTemplate().getForEntity("http://localhost:8081/users/"+id, UserDTO.class);
			return resp.getBody().getPortefeuille();
		}
		private int getCartePrice(int id) {
			ResponseEntity<CarteDTO> resp = new RestTemplate().getForEntity("http://localhost:8082/cartes/"+id, CarteDTO.class);
			return resp.getBody().getPrice();
		}
		
		
		private void putUserPortefeuille(int id, int price) {
			new RestTemplate().exchange("http://localhost:8081/users/"+id+"/portefeuille?price="+price, HttpMethod.PUT, null, Void.class);
		}                                                    
		
		private void deleteUserCarte(int idUser, int idCarte) {
			System.out.println("idUser"+idUser+"+"+idCarte+"-----------------------------------------------------------------------");
			System.out.println("http://localhost:8081/users/"+idUser+"/cartes?id="+idCarte);
			new RestTemplate().exchange("http://localhost:8081/users/"+idUser+"/cartes?idCarte="+idCarte, HttpMethod.DELETE, null, Void.class);
		}
		private void postUserCarte(int idUser, int idCarte) {
			new RestTemplate().exchange("http://localhost:8081/users/"+idUser+"/cartes?idCarte="+idCarte, HttpMethod.POST, null, Void.class);
		}
		private void putOwnerCarte(int idOwner, int idCarte) {
			new RestTemplate().exchange("http://localhost:8082/cartes?idCarte="+idCarte+"&idOwner="+idOwner, HttpMethod.PUT, null, Void.class);
		}
		
		public static class UserDTO {
			public int portefeuille;

			public UserDTO() {}
			
			public int getPortefeuille() {
				return portefeuille;
			}

			public void setPortefeuille(int portefeuille) {
				this.portefeuille = portefeuille;
			}

		}
		
		public static class CarteDTO {
			public int price;

			public CarteDTO() {}
			
			public int getPrice() {
				return price;
			}
			
			
			public void setPrice(int price) {
				this.price = price;
			}
		}
		
	}
