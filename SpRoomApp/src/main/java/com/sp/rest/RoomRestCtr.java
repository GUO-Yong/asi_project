package com.sp.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sp.model.Room;
import com.sp.service.RoomService;

@RestController
@RequestMapping("/rooms")
public class RoomRestCtr {
	
	@Autowired
	private RoomService rService;
	
	
	@GetMapping
	public List<Room> getAllRooms() {
		return rService.getAllRooms();
	}
	
	/**
	 * But : Récupérer une room via son Id 
	 * @param id1 de la room 
	 * @return Room 
	 */
	@GetMapping(value="/{id1}")
	public Room getRoomById(@PathVariable String id1) {
		return rService.getRoomById(Integer.valueOf(id1));
		
	}
	
	/**
	 * But : Filtrée les rooms via le prix de la mise 
	 * @param price
	 * @return
	 */
	@GetMapping(value="/prices/{price}")
	public List<Room> getRoomsByPrice(@PathVariable String price) {
		return rService.getByPrice(Integer.valueOf(price));
	}
	
	/**
	 * But : Création d'une Room par un utilisateur 
	 * @param idOwner
	 * @param name
	 * @param price
	 */
	@PostMapping
	public void addRoom(@RequestParam int idOwner, @RequestParam String name, @RequestParam String price ) {
		rService.addRoom(idOwner, name, Integer.valueOf(price));
	}
	
	/**
	 * But : Ajout d'un visiteur dans la room
	 * @param idRoom
	 * @param idVisitor
	 */
	@PutMapping
	public void putRoom(@RequestParam int idRoom, @RequestParam int idVisitor) {
		rService.completRoom(idRoom, idVisitor);
		
	}

}
