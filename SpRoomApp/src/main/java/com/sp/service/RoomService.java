package com.sp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.sp.model.Room;
import com.sp.repository.RoomRepository;

@Service
public class RoomService {
	
	@Autowired
	RoomRepository rRepository;
	
	/**
	 * But : Création d'une Room par un utilisateur avec son nom et son prix 
	 * @param idOwner
	 * @param name
	 * @param price
	 */
	public void addRoom(int idOwner, String name, int price) {
		ResponseEntity<UserDTO> resp = new RestTemplate().getForEntity("http://localhost:8081/users/"+idOwner, UserDTO.class);
		System.out.println(resp.getBody().getPortefeuille());
		System.out.println(resp);
		if(resp.getBody().getPortefeuille() > price) {
			Room createdRoom=rRepository.save(new Room(idOwner, name, price));
			System.out.println(createdRoom);
		} else {
			System.out.println("Pas assez riche");
		}
	}
	
	/**
	 * But : Récupération de toutes les rooms 
	 * @return liste des rooms (toutes)
	 */
	public List<Room> getAllRooms() {
		List<Room> rooms = new ArrayList<>();
		rRepository.findAll().forEach(rooms::add);
	    return rooms;
	}
	
	
	/**
	 * But : retourne une room via son id
	 * @param id de la room
	 * @return room 
	 */
	public Room getRoomById(int id) {
		Optional<Room> rOpt =rRepository.findById(id);
		if (rOpt.isPresent()) {
			return rOpt.get();
		}else {
			return null;
		}
	}
	
	/**
	 * But : retourne les salles en fonction d'un prix 
	 * @param prix d'entrée de la salle 
	 * @return liste des rooms filtrées
	 */
	public List<Room> getByPrice(int price) {
		List<Room> rooms = new ArrayList<>();
		List<Room> roomsByPrice = new ArrayList<Room>();
		rRepository.findAll().forEach(rooms::add);
		for (int k=0; k < rooms.size(); k++) {
			 Room rtest = rooms.get(k);
			 if ( rtest.getPrice() == price ) roomsByPrice.add(rtest);
		}
	    return roomsByPrice;
	}
	
	/**
	 * But : Modifier une room pour ajouter un visiteur
	 * @param idRoom
	 * @param idVisitor
	 */
	public void completRoom(int idRoom,int idVisitor) {
		Optional<Room> rOpt =rRepository.findById(idRoom);
		if (rOpt.isPresent() ) {
			Room roomaComplet = rOpt.get();
			ResponseEntity<UserDTO> resp = new RestTemplate().getForEntity("http://localhost:8081/users/"+idVisitor, UserDTO.class);
			if(resp.getBody().getPortefeuille() >= roomaComplet.getPrice() && idVisitor != roomaComplet.getId()) {
				roomaComplet.setIdVisitor(idVisitor);
				roomaComplet.setPeutCommencer(true);
				rRepository.save(roomaComplet);
	/*TODO Game
			//appeler le  service game
				  GameStart( idRoom,   roomaComplet.getIdOwner(), 
						 idVisitor, roomaComplet idCarteOwner,  idCarteVisitor )
	*/
			} else {
				System.out.println("Pas assez riche/Bad vistor");
			}			
		}
	}
	
	//appeler le service Game
	public void GameStart(Integer idRoom,  Integer idOwner, 
			Integer idVisitor, Integer idCarteOwner, Integer idCarteVisitor ) {
		new RestTemplate().exchange("http://localhost:8087/games?idRoom="+idRoom+"&idOwner"
				+"&idVisitor"+idVisitor+"&idCarteOwner"+idCarteOwner+"&idCarteVisitor"+idCarteVisitor,
				HttpMethod.PUT, null, Void.class);
	}
	
	//selectionner aleatoirement une id carte que  l'utilisateur possede
	public int SelectRandomCard(UserDTO userDTO) {
		List<Integer> listeCartes =  userDTO.getListeCartes();
		return listeCartes.get((int)Math.random()*listeCartes.size()+1);
	}
	
	
	public static class UserDTO {
		public int portefeuille;

		public List<Integer> listeCartes;
		public UserDTO() {}
		
		public int getPortefeuille() {
			return portefeuille;
		}

		public void setPortefeuille(int portefeuille) {
			this.portefeuille = portefeuille;
		}
		public List<Integer> getListeCartes() {
			return listeCartes;

	}


}

}
