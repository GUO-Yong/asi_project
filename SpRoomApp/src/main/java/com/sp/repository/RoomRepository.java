package com.sp.repository;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import com.sp.model.Room;

public interface RoomRepository extends CrudRepository<Room, Integer>{
	public List<Room> findByName(String name);

}
