package com.sp.service;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.sp.model.Room;
import com.sp.repository.RoomRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(value = RoomService.class)
public class RoomServiceTest {

	@Autowired
	private RoomService rService;

	@MockBean
	private RoomRepository rRepo;
	
	Room tempRoom=new Room(1,"test",1);
	
	@Test
	public void getgetRoomById() {
		Mockito.when(
				rRepo.findById(Mockito.any())
				).thenReturn(Optional.ofNullable(tempRoom));
		Room tInfo=rService.getRoomById(45);
		assertTrue(tInfo.toString().equals(tempRoom.toString()));
	}
	
}
