
package com.sp.repository;
import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import com.sp.model.Game;


public interface GameRepository extends CrudRepository<Game, Integer>{
	public Optional<Game> findByIdRoom(Integer id);

}
