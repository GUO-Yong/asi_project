package com.sp.model;

import java.util.List;

public class Joueur {
	private int idJoueur;
	private int idCarte;
	private List<String> token;
	public Joueur(int idJoueur, int idCarte, String token) {
		super();
		this.idJoueur = idJoueur;
		this.idCarte = idCarte;
		this.token.add(token) ;
	}
	public int getIdJoueur() {
		return idJoueur;
	}
	public void setIdJoueur(int idJoueur) {
		this.idJoueur = idJoueur;
	}
	public int getIdCarte() {
		return idCarte;
	}
	public void setIdCarte(int idCarte) {
		this.idCarte = idCarte;
	}
	public List<String> getToken() {
		return token;
	}
	public void setToken(List<String> token) {
		this.token = token;
	}

	

}
