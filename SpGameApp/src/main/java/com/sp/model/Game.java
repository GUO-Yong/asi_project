package com.sp.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Game {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private Integer idRoom;
	private boolean switchJoueur;
	private int nbCycle = 0 ;
	private String messageCycle;
	private Integer winner = null;
	
	private Joueur owner;
	private Joueur visitor;
	
	private int vieCarteOwner;
	private int attaqueOwner;
	
	private int vieCarteVisitor;
	private int attaqueVisitor;
	
	public Game(Integer idRoom, Integer idOwner, Integer idVisitor, Integer idCarteOwner, Integer idCarteVisitor) {
		super();
		this.idRoom = idRoom;
		this.owner = new Joueur(idOwner, idCarteOwner , ""+ idRoom+idOwner+"0");
		this.owner = new Joueur(idVisitor, idCarteVisitor , ""+ idRoom+idCarteVisitor+"0");
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getIdRoom() {
		return idRoom;
	}

	public void setIdRoom(Integer idRoom) {
		this.idRoom = idRoom;
	}

	public boolean isSwitchJoueur() {
		return switchJoueur;
	}

	public void setSwitchJoueur(boolean switchJoueur) {
		this.switchJoueur = switchJoueur;
	}

	public Integer getWinner() {
		return winner;
	}

	public void setWinner(Integer winner) {
		this.winner = winner;
	}

	public Joueur getOwner() {
		return owner;
	}

	public void setOwner(Joueur owner) {
		this.owner = owner;
	}

	public Joueur getVisitor() {
		return visitor;
	}

	public void setVisitor(Joueur visitor) {
		this.visitor = visitor;
	}

	public int getVieCarteOwner() {
		return vieCarteOwner;
	}

	public void setVieCarteOwner(int vieCarteOwner) {
		this.vieCarteOwner = vieCarteOwner;
	}

	public int getAttaqueOwner() {
		return attaqueOwner;
	}

	public void setAttaqueOwner(int attaqueOwner) {
		this.attaqueOwner = attaqueOwner;
	}

	public int getVieCarteVisitor() {
		return vieCarteVisitor;
	}

	public void setVieCarteVisitor(int vieCarteVisitor) {
		this.vieCarteVisitor = vieCarteVisitor;
	}

	public int getAttaqueVisitor() {
		return attaqueVisitor;
	}

	public void setAttaqueVisitor(int attaqueVisitor) {
		this.attaqueVisitor = attaqueVisitor;
	}

	public int getNbCycle() {
		return nbCycle;
	}

	public void setNbCycle(int nbCycle) {
		this.nbCycle = nbCycle;
	}

	public String getMessageCycle() {
		return messageCycle;
	}

	public void setMessageCycle(String messageCycle) {
		this.messageCycle = messageCycle;
	}
}
