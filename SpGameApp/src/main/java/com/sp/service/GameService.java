package com.sp.service;

import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.sp.model.Game;
import com.sp.model.Joueur;
import com.sp.repository.GameRepository;


@Service
public class GameService {
	@Autowired
	GameRepository gRepository;
	
	public void addGame(Integer idRoom, Integer idOwner,
			Integer idVisitor,Integer idCarteOwner,Integer idCarteVisitor) {
		Game game = new Game( idRoom,  idOwner, idVisitor,  idCarteOwner,  idCarteVisitor);
		gRepository.save(game);
		
	}
	
	private Game getGameByIdRoom(int id) {
		Optional<Game> rOpt = gRepository.findByIdRoom(id);
		if (rOpt.get() != null ) {
			return rOpt.get();
		}else {
			return null;
		}
	}
	
	
	public void GameCycle(Integer idRoom, Integer idOwner, Integer idVisitor,Integer idCarteOwner,Integer idCarteVisitor) {

		Game game =getGameByIdRoom(idRoom);
		Random rd = new Random(); 
	    String message = "";
	    if (game.getNbCycle() == 0) generFirstCycle(game);
		if (game.getWinner() == null ) {
			if (rd.nextBoolean()) {
				game.setVieCarteOwner(game.getVieCarteOwner()-game.getAttaqueVisitor());
				message = "La carte Visiteur perd "+game.getAttaqueVisitor()+", il lui reste : "+game.getVieCarteOwner();
				finDeJeu(game.getVieCarteOwner(), game.getVisitor().getIdJoueur(),game);
				game.setVieCarteVisitor(game.getVieCarteVisitor()-game.getAttaqueOwner());
				message += "La carte Visiteur perd "+game.getAttaqueOwner()+", il lui reste : "+game.getVieCarteVisitor();
				finDeJeu(game.getVieCarteVisitor(), game.getOwner().getIdJoueur(),game);
			} else {
				game.setVieCarteVisitor(game.getVieCarteVisitor()-game.getAttaqueOwner());
				message = "La carte Visiteur perd "+game.getAttaqueOwner()+", il lui reste : "+game.getVieCarteVisitor();
				finDeJeu(game.getVieCarteVisitor(), game.getOwner().getIdJoueur(),game);
				game.setVieCarteOwner(game.getVieCarteOwner()-game.getAttaqueVisitor());
				message += "La carte Visiteur perd "+game.getAttaqueVisitor()+", il lui reste : "+game.getVieCarteOwner(); 
				finDeJeu(game.getVieCarteOwner(), game.getVisitor().getIdJoueur(),game);
			}
			game.setNbCycle(game.getNbCycle()+1);
		}
	}

		
		
	private boolean finDeJeu(int vieDeCarte, Integer idJoueurAdversaire,Game game) {
		boolean ret = false;
		if (vieDeCarte < 0) {
			game.setWinner(idJoueurAdversaire);
			ret = true;
		}
		return ret;
	}
	
	private void generFirstCycle(Game game) {
		Joueur owner = game.getOwner();
		Joueur vistor = game.getVisitor();
		
		CarteDTO carteOwner  =getCarte(owner.getIdJoueur());
		CarteDTO carteVisitor =getCarte(vistor.getIdJoueur());

		game.setAttaqueOwner(carteOwner.getAttaque());
		game.setAttaqueVisitor(carteVisitor.getAttaque());
		game.setVieCarteOwner(carteOwner.getHp());
		game.setVieCarteVisitor(carteVisitor.getHp());
	}
	
	private String receptionToken(String token, int idRoom, int idJoueur) {
		Game game =getGameByIdRoom(idRoom);
		if (game.getOwner().getIdJoueur() == idJoueur) {
			
		}
		return "";
	}
	
	private CarteDTO getCarte(int id) {
		ResponseEntity<CarteDTO> resp = new RestTemplate().getForEntity("http://localhost:8082/cartes/"+id, CarteDTO.class);
		return resp.getBody();
	}
	
	public static class CarteDTO {
		public int hp;
		public int energy;
		public int defense;
		public int attaque;
		
		public CarteDTO() {}
		
		public int getHp() {
			return hp;
		}
		public int getEnergy() {
			return energy;
		}
		public int getDefense() {
			return defense;
			
		}
		public int getAttaque() {
			return attaque;
		}
		public void setHp(int hp) {
			this.hp = hp;
		}
	}
}
