function achatCarte() {
	var idacheteur = document.getElementById("idJoueur").value
	var id = $_GET('id')
	let params = {
  		"idacheteur": idacheteur,
  		"id": id
	};
	console.log(params);
	
	let query = Object.keys(params)
	             .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
	             .join('&');
	
	let url = 'http://localhost:8082/transactions?' + query;
	
	fetch(url,{
	  headers: {
	    'Accept': 'application/json',
	    'Content-Type': 'application/json'
	  },
	  method: "PUT",
	  body: JSON.stringify(params)
	})
	.then(response => response.json())
	.then(response => callbackTrois(response))
	.catch(error => err_callbackTrois(error))
}

function callbackTrois(response) {
	if(response) {
		document.getElementById("result").innerHTML = "<span style='color:green'> Vous avez achetez cette carte </span>"
		document.location.href="transactions.html?idconnection="+idacheteur;
	} else {
		document.getElementById("result").innerHTML = "<span style='color:red'> Vous n'avez pas la somme nécessaire</span>"
	}
}

function err_callbackTrois(error){
    console.log(error);
}

function $_GET(param) {
	var vars = {};
	window.location.href.replace( location.hash, '' ).replace( 
		/[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
		function( m, key, value ) { // callback
			vars[key] = value !== undefined ? value : '';
		}
	);
	if ( param ) {
		return vars[param] ? vars[param] : null;	
	}
	return vars;
}