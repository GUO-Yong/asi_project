/**
 * 
 */

var idconnection = $_GET("idconnection")
var idcarte = $_GET("idcarte")
let params = {
	"idconnection": idconnection ,
	"idcarte": idcarte
};

var context = {
  method: "POST",
}

let query = Object.keys(params)
             .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
             .join('&');


let GET_URL = 'http://localhost:8082/transactions?' + query;    

fetch(GET_URL,context)
    .then(response => response.json())
        .then(response => callback(response))
        .catch(error => err_callback(error));

function callback(response){
	console.log(response);
	document.location.href="transactions.html?idconnection="+idconnection;
}

function err_callback(error){
    console.log(error);
}

function $_GET(param) {
	var vars = {};
	window.location.href.replace( location.hash, '' ).replace( 
		/[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
		function( m, key, value ) { // callback
			vars[key] = value !== undefined ? value : '';
		}
	);

	if ( param ) {
		return vars[param] ? vars[param] : null;	
	}
	return vars;
}