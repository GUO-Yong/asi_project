function cinqCartes() {
		
	const GET_URL="http://localhost:8082/getFiveCards"; 
    let context =   {
                        method: 'GET'
                    };
        
    fetch(GET_URL,context)
        .then(response => response.json())
            .then(response => callback(response))
            .catch(error => err_callback(error));
}

function callback(response){
    console.log(response);
    //document.getElementById("cinqCartes").innerHTML = JSON.stringify(response,undefined,2);
    addFirstCartes(response[0]);
    addCartes(response[1]);
    addCartes(response[2]);
    addCartes(response[3]);
    addCartes(response[4]);
    
    const someData = {
        idconnection: $_GET('idconnection'),
        carteUne:JSON.stringify(response[0]),
        carteDeux:JSON.stringify(response[1]),
        carteTrois:JSON.stringify(response[2]),
        carteQuatre:JSON.stringify(response[3]),
        carteCinq:JSON.stringify(response[4])
    }
    console.log(someData);
    const putMethod = {
        method: 'PUT', // Method itself
        headers: {
         'Content-type': 'application/json; charset=UTF-8' // Indicates the content 
        },
        body: JSON.stringify(someData) // We send data in JSON format
       }
       
       let query = Object.keys(someData)
	             .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(someData[k]))
	             .join('&');
       
        console.log(query);
       // make the HTTP put request using fetch api
       fetch("http://localhost:8082/usersaddcartes?"+query, putMethod)
}

function err_callback(error){
    console.log(error);
}

function addFirstCartes(response) {
    let card = {
        family_src:response['family_src'],
        family_name:response['family_name'],
        img_src:response['img_src'],
        name:response['name'],
        description:response['description'],
        hp:response['hp'],
        energy:response['energy'],
        attack:response['attack'],
        defense: response['defense']  
    };
    let template = document.querySelector("#selectedCard");
    let clone = document.importNode(template.content, true);

    newContent= clone.firstElementChild.innerHTML
        .replace(/{{family_src}}/g, card.family_src)
        .replace(/{{family_name}}/g, card.family_name)
        .replace(/{{img_src}}/g, card.img_src)
        .replace(/{{name}}/g, card.name)
        .replace(/{{description}}/g, card.description)
        .replace(/{{hp}}/g, card.hp)
        .replace(/{{energy}}/g, card.energy)
        .replace(/{{attack}}/g, card.attack)
        .replace(/{{defense}}/g, card.defense);
    clone.firstElementChild.innerHTML= newContent;

    let cardContainer= document.querySelector("#card");
    cardContainer.replaceWith(clone);
}

function addCartes(response) {
    let card = {
        family_src:response['family_src'],
        family_name:response['family_name'],
        img_src:response['img_src'],
        name:response['name'],
        description:response['description'],
        hp:response['hp'],
        energy:response['energy'],
        attack:response['attack'],
        defense: response['defense']  
    };
    let template = document.querySelector("#selectedCard");
    let clone = document.importNode(template.content, true);

    newContent= clone.firstElementChild.innerHTML
        .replace(/{{family_src}}/g, card.family_src)
        .replace(/{{family_name}}/g, card.family_name)
        .replace(/{{img_src}}/g, card.img_src)
        .replace(/{{name}}/g, card.name)
        .replace(/{{description}}/g, card.description)
        .replace(/{{hp}}/g, card.hp)
        .replace(/{{energy}}/g, card.energy)
        .replace(/{{attack}}/g, card.attack)
        .replace(/{{defense}}/g, card.defense);
    clone.firstElementChild.innerHTML= newContent;

    let cardContainer= document.querySelector("#cardContainer");
    cardContainer.appendChild(clone);
}

function $_GET(param) {
	var vars = {};
	window.location.href.replace( location.hash, '' ).replace( 
		/[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
		function( m, key, value ) { // callback
			vars[key] = value !== undefined ? value : '';
		}
	);

	if ( param ) {
		return vars[param] ? vars[param] : null;	
	}
	return vars;
}