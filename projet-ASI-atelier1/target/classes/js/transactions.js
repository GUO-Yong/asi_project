const GET_URL="http://localhost:8082/transactions"; 
let context =   {
                    method: 'GET'
                };
    
fetch(GET_URL,context)
    .then(response => response.json())
        .then(response => callback(response))
        .catch(error => err_callback(error));


function callback(response){
    ret="<table><tr><td>Page</td><td>Carte</td><td>Vendeur</td><td>Prix</td></tr>"
    for (let i = 0; i < response.length; i++) {
        ret = ret+ "<tr><td><a href=transaction.html?id="+response[i]["id"]+"&idconnection="+$_GET('idconnection')+">ici</a></td><td>"+response[i]["carte"]["name"]+"</td><td>"+response[i]["vendeur"]["nom"]+"</td><td>"+response[i]["carte"]["price"]+"</td></tr>"
    }
    ret = ret + "</table>"
    document.getElementById("allTransactions").innerHTML = ret
}

function err_callback(error){
    console.log(error);
}

function $_GET(param) {
	var vars = {};
	window.location.href.replace( location.hash, '' ).replace( 
		/[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
		function( m, key, value ) { // callback
			vars[key] = value !== undefined ? value : '';
		}
	);

	if ( param ) {
		return vars[param] ? vars[param] : null;	
	}
	return vars;
}