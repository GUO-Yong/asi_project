const GET_URL_PROFIL="http://localhost:8082/usersname/"; 
let contexte =   {
                    method: 'GET'
                };
    
fetch(GET_URL_PROFIL+$_GET('idconnection'),contexte)
    .then(response => response.json())
        .then(response => callbackdeux(response))
        .catch(error => err_callbackdeux(error));


function callbackdeux(response){	
    document.getElementById("portemonnaie").innerHTML = response.portefeuille;
	document.getElementById("idJoueur").value = response.id;
}

function err_callbackdeux(error){
    console.log(error);
}

const GET_URL="http://localhost:8082/transactions/"; 
let context =   {
                    method: 'GET'
                };
    
fetch(GET_URL+$_GET('id'),context)
	.then(response=> response.json())
        .then(response => callback(response))
        .catch(error => err_callback(error));


function callback(transaction){
	let template = document.querySelector("#selectedCard")
	let clone = document.importNode(template.content, true);
    newContent= clone.firstElementChild.innerHTML
            .replace(/{{family_src}}/g, transaction.carte.family_src)
            .replace(/{{family_name}}/g, transaction.carte.family_name)
            .replace(/{{img_src}}/g, transaction.carte.img_src)
            .replace(/{{name}}/g, transaction.carte.name)
            .replace(/{{description}}/g, transaction.carte.description)
            .replace(/{{hp}}/g, transaction.carte.hp)
            .replace(/{{energy}}/g, transaction.carte.energy)
            .replace(/{{attack}}/g, transaction.carte.attack)
            .replace(/{{defense}}/g, transaction.carte.defense);
        clone.firstElementChild.innerHTML= newContent;
    
    let cardContainer= document.querySelector("#cardContainer");
        cardContainer.appendChild(clone);
	document.getElementById("nomDuJoueur").innerHTML = transaction.vendeur.nom;
	document.getElementById("prixCarte").innerHTML = transaction.carte.price;	
}

function err_callback(error){
    console.log(error);
}

function $_GET(param) {
	var vars = {};
	window.location.href.replace( location.hash, '' ).replace( 
		/[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
		function( m, key, value ) { // callback
			vars[key] = value !== undefined ? value : '';
		}
	);
	if ( param ) {
		return vars[param] ? vars[param] : null;	
	}
	return vars;
}