const GET_CHUCK_URL="http://localhost:8082/cartes/"+$_GET('id'); 
    let context =   {
                        method: 'GET'
                    };
        
    fetch(GET_CHUCK_URL,context)
        .then(response => response.json())
            .then(response => callback(response))
        	.catch(error => err_callback(error));


function callback(response){
    let card = {
        family_src:response['family_src'],
        family_name:response['family_name'],
        img_src:response['img_src'],
        name:response['name'],
        description:response['description'],
        hp:response['hp'],
        energy:response['energy'],
        attack:response['attack'],
        defense: response['defense']  
    };
    let template = document.querySelector("#selectedCard");
    let clone = document.importNode(template.content, true);

    newContent= clone.firstElementChild.innerHTML
        .replace(/{{family_src}}/g, card.family_src)
        .replace(/{{family_name}}/g, card.family_name)
        .replace(/{{img_src}}/g, card.img_src)
        .replace(/{{name}}/g, card.name)
        .replace(/{{description}}/g, card.description)
        .replace(/{{hp}}/g, card.hp)
        .replace(/{{energy}}/g, card.energy)
        .replace(/{{attack}}/g, card.attack)
        .replace(/{{defense}}/g, card.defense);
    clone.firstElementChild.innerHTML= newContent;

    let cardContainer= document.querySelector("#cardContainer");
    cardContainer.replaceWith(clone);
}

function err_callback(error){
    console.log(error);
}

function $_GET(param) {
	var vars = {};
	window.location.href.replace( location.hash, '' ).replace( 
		/[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
		function( m, key, value ) { // callback
			vars[key] = value !== undefined ? value : '';
		}
	);

	if ( param ) {
		return vars[param] ? vars[param] : null;	
	}
	return vars;
}