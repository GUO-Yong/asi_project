function generate() {
	var idconnection = document.getElementById("idconnection").value
	var mdp = document.getElementById("mdp").value
	let params = {
  		"idconnection": idconnection ,
  		"mdp": mdp
	};
	console.log(params);
	
	let query = Object.keys(params)
	             .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
	             .join('&');
	
	let url = 'http://localhost:8082/userconnection?' + query;
	
	fetch(url,{
	  headers: {
	    'Accept': 'application/json',
	    'Content-Type': 'application/json'
	  },
	  method: "POST",
	  body: JSON.stringify(params)
	})
	.then(response => response.json())
	.then(response => callback(response))
	.catch(error => err_callback(error));
	}
	
	function callback(response){
		if (!(response['idConnexion'] == "Inconnu")) {
			document.location.href="transactions.html?idconnection="+response['idConnexion'];
		} else {
			document.getElementById('txtChuck').innerHTML = "<span style='color:red'> Erreur authentification </span>"
		}
	}
	
	function err_callback(error){
	console.log(error);
}