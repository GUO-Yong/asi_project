package com.sp.rest;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper; 
import com.fasterxml.jackson.databind.ObjectWriter;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sp.model.User;
import com.sp.service.UserService;

@RestController
public class UserRestCtrl {
	
    @Autowired
    UserService uService;
    
	
	@RequestMapping(method=RequestMethod.GET,value="/users/construction")
	public void UserSet() {
		for(int j =0; j<4;j++) {
		User user = new User( "A"+j,  "A"+j, j+50, "AAAAA", "AAAAA");
		List<Integer> list = uService.getFiveCards(user.getid());
		uService.createDeck(user);
		for (int i=0; i<list.size();i++) {
			uService.addCarte(user, list.get(i));
		}
		uService.addUser(user);
		System.out.println("add user:"+user);
		}
		
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/users")
	public void addUser(@RequestParam String nom, @RequestParam String prenom, @RequestParam String mdp, @RequestParam String idconnection) {
		User user = new User( nom,  prenom, 0, mdp, idconnection);
		List<Integer> list = uService.getFiveCards(user.getid());
		uService.createDeck(user);
		for (int i=0; i<list.size();i++) {
			uService.addCarte(user, list.get(i));
		}
		uService.addUser(user);
		System.out.println("add user:"+user);
		// Astuce pour rediriger vers une page html
		//return "<meta http-equiv=\"refresh\" content=\"0;URL=choixcartes.html?idconnection="+idconnection+"\">";
	}

	
	@RequestMapping(method=RequestMethod.GET,value="/users/{idUser}")
	public User getUserById(@PathVariable String idUser) {
		 User u = uService.getUser(Integer.valueOf(idUser));
         return u;
	}
	
	
	@RequestMapping(method=RequestMethod.GET,value="/users/idconnection/{idconnection}")
	public User getUserByNom(@PathVariable String idconnection) {
		 User u = uService.getUser(idconnection);
         return u;
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/users/{idUser}/cartes")
	public List<Integer> getDeckListByID(@PathVariable int idUser) {
		return uService.getUserDeckList(idUser);    
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/users")
	public List<User> getAllUser() {
         return uService.getAllUser();
	}

	@RequestMapping(method = RequestMethod.POST,value = "/users/connection",produces = MediaType.APPLICATION_JSON_VALUE)
	public  String verifUser(@RequestParam String idconnection, @RequestParam String mdp){
		User user = uService.getUser(idconnection, mdp);
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		String json="";
		try {
			json = ow.writeValueAsString(user);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(json);
		return json;
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/users/start")
	public String addUsersMultiple() {
		User user = new User("Paul", "Paul",0,"azerty123","Paul69420");
		uService.addUser(user);
		user = new User("Jean", "Martin",0,"motdepasse69","JM_la_street");
		uService.addUser(user);
		user = new User("Charrat", "Phillips",1000000,"Troplong111","Philou_du_38!");
		uService.addUser(user);
		user = new User("Kroky", "A",69,"TuMeTrOuvErAsPaS","Mexikroky");
		uService.addUser(user);
		return "Ajout des utilisateurs";
	}
	
	@RequestMapping(method=RequestMethod.DELETE,value="/users/{idUser}/cartes")
	public void RemoveCarte(@PathVariable int idUser, @RequestParam int idCarte){
		User user = uService.getUser(idUser);
		user.getListeCartes().remove(Integer.valueOf(idCarte));	
		uService.addUser(user);
		System.out.println("remove carte"+idCarte);
	}
	@RequestMapping(method=RequestMethod.POST,value="/users/{idUser}/cartes")
	public void AddCarte(@PathVariable int idUser, @RequestParam int idCarte){
		User user = uService.getUser(idUser);
		user.getListeCartes().add(idCarte);	
		uService.addUser(user);
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/users/{idUser}/portefeuille")
	public void setPortefeuille(@PathVariable int idUser, @RequestParam int price) {
		User user = uService.getUser(idUser);
		user.setPortefeuille(price);
		uService.addUser(user);
	}	
	

}
