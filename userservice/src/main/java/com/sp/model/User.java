package com.sp.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class User {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private String nom;
	private String prenom;
	private int portefeuille;
	private String mdp;
	private String idConnexion;
	
	@ElementCollection
	private List<Integer> listeidCartes = new ArrayList<>();
	
	public User() {}
	public User(String nom, String prenom, int portefeuille, String mdp, String idConnexion) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.portefeuille = portefeuille;
		this.mdp = mdp;
		this.idConnexion = idConnexion;
	}

	/**
	 * But : récupère le mdp du l'utilisateur 
	 * @param 
	 * @return le mdp de l'utilisateur
	 */
	public String getMdp() {
		return mdp;
	}
	/**
	 * But : change le mdp de l'utilisateur
	 * @param mdp le nouveau mdp de l'utilisateur
	 * @return /
	 */
	public void setMdp(String mdp) {
		this.mdp = mdp;
	}
	/**
	 * But : récupère l'id de connexion de l'utilisateur
	 * @param 
	 * @return l'id de connexion de l'utilisateur
	 */
	public String getIdConnexion() {
		return idConnexion;
	}
	
	/**
	 * But : modifie l'id de connexion de l'utilisateur
	 * @param le nouvel id de connexion de l'utilisateur
	 * @return 
	 */
	public void setIdConnexion(String idConnexion) {
		this.idConnexion = idConnexion;
	}
	
	/**
	 * But : récupère l'id de l'utilisateur
	 * @param 
	 * @return l'id de l'utilisateur
	 */
	public int getid() {
		return id;
	}

	public List<Integer> getListeCartes() {
		return listeidCartes;
	}
	public void setListeCartes(List<Integer> listeidCartes) {
		this.listeidCartes = listeidCartes;
	}
	/**
	 * But : récupère le nom de l'utilisateur
	 * @param 
	 * @return le nom de l'utilisateur
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * But : modifie le nom de l'utilisateur
	 * @param nom le nouveau nom de l'utilisateur
	 * @return 
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * But : récupère le prénom de l'utilisateur
	 * @param 
	 * @return le prénom de l'utilisateur
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * But : modifie le prénom de l'utilisateur
	 * @param prenom le prénom de l'utilisateur
	 * @return 
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/**
	 * But : récupère la valeur du portefeuille de l'utilisateur
	 * @param 
	 * @return la valeur du portefeuille de l'utilisateur
	 */
	public int getPortefeuille() {
		return portefeuille;
	}

	/**
	 * But : modifie la valeur du portefeuille de l'utilisateur
	 * @param portefeuille la nouvelle valeur du portefeuille
	 * @return 
	 */
	public void setPortefeuille(int somme) {
		this.portefeuille = this.portefeuille + somme;
	}
	
	/**
	 * But : ajoute une carte à la liste des cartes possédées par l'utilisateur
	 * @param carte la nouvelle carte à ajouter
	 * @return 
	 */
	public void addCarte(int idcarte) {
		this.listeidCartes.add(idcarte);
	}
	
	/**
	 * But : enlève une carte à la liste des cartes possédées par l'utilisateur
	 * @param carte la carte à enlever
	 * @return 
	 */
	public void removeCarte(int idcarte) {
		this.listeidCartes.remove(idcarte);
	}

	/**
	 * But : renvoie une chaine de caractère contenant tous les attributs de l'utilisateur
	 * @param 
	 * @return la chaine de caractère contenant tous les attributs de l'utilisateur 
	 */
	@Override
	public String toString() {
		String ret = "{ id:" + id + 
				", nom:" + nom + ", prenom:" + prenom + ", portefeuille:" + portefeuille + 
				",idConnexion: " + idConnexion + ", deckList:" +listeidCartes+ "}";
		return ret;
		
	}
}
