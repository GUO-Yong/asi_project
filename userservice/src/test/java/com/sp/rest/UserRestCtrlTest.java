package com.sp.rest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.sp.model.User;
import com.sp.service.UserService;

@RunWith(SpringRunner.class)
@WebMvcTest(value = UserRestCtrl.class)
public class UserRestCtrlTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private UserService hService;

	User mockUser=new User("Paul1", "Paul",0,"azerty123","Paul69420");
	
	@Test
	public void retrieveUser() throws Exception {
		Mockito.when(
				hService.getUser(Mockito.anyInt())
				).thenReturn(mockUser);
				

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/users/50").accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		System.out.println(result.getResponse().getContentAsString());
		//String expectedResult="{\"id\":1,\"name\":\"jdoe\",\"superPowerName\":\"strong\",\"superPowerValue\":100,\"imgUrl\":\"https//url.com\"}";

		String expectedResult="{id:0,nom:Paul1,prenom:Paul,portefeuille:0}";
		JSONAssert.assertEquals(expectedResult, result.getResponse()
				.getContentAsString(), false);
		
		
	}

}
