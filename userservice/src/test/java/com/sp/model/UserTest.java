package com.sp.model;

import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class UserTest {
	private List<String> idConnexionList;
	private List<String> mdpList;
	private List<Integer> portefeuilleList;
	

	@Before
	public void setUp() {
		System.out.println("[BEFORE TEST] -- Add User to test");
		mdpList = new ArrayList<String>();
		idConnexionList = new ArrayList<String>();
		portefeuilleList = new ArrayList<Integer>();
		idConnexionList.add("user1");
		idConnexionList.add("user2");
		idConnexionList.add("user3");
		mdpList.add("123");
		mdpList.add("456");
		mdpList.add("789");
		portefeuilleList.add(0);
		portefeuilleList.add(69);
		portefeuilleList.add(500);
	}

	@After
	public void tearDown() {
		System.out.println("[AFTER TEST] -- CLEAN hero list");
		idConnexionList = null;
		idConnexionList = null;
		portefeuilleList = null;
	}
	

	
	@Test
	public void createUser() {
		int size = idConnexionList.size();
		for(int i = 0; i< size; i++) {
			String nom ="TOTO"; String prenom="YOYO"; 
			
			int portefeuille =portefeuilleList.get(i) ;
			String idConnexion = idConnexionList.get(i);
			String mdp = mdpList.get(i);
			User h=new User( nom,  prenom,  portefeuille,  mdp,  idConnexion);
			System.out.println("nom :"+nom+", prenom:" + prenom +", portefeuille:"+portefeuille+", mdp:"+mdp+", idConnexion:"+idConnexion);
			assertTrue(h.getPortefeuille() == portefeuille);
			
		}
	}
	
	@Test
	public void displayUser() {
		User h= new User("Paul", "Paul",0,"azerty123","Paul69420");
		String expectedResult="{ id:0, nom:Paul, prenom:Paul, portefeuille:0,idConnexion: Paul69420, deckList:[]}";
		assertTrue(h.toString().equals(expectedResult));
	}
}
